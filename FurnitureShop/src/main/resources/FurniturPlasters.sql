
drop database  Furniture ;
CREATE database Furniture;
use furniture;

   
DROP  TABLE IF EXISTS user;
CREATE TABLE user(
	id INT auto_increment,

	username VARCHAR(40),
    user_type VARCHAR(20),
    password VARCHAR(100),
    email VARCHAR(50),
    PRIMARY KEY (`id`)  
    
);



insert into user
values (null,'Joe','Customer','password','Joe@email.ie');
insert into user
values (null,'Bridget','Picker','password','Bridget@email.ie');

DROP TABLE IF EXISTS Plasters;

CREATE TABLE Plasters(
	id INT auto_increment,
	name VARCHAR(40),
    image VARCHAR(20),
	 price INT,
    PRIMARY KEY (`id`)
);

insert into Plasters values (null,'Abraham','Abraham.jpg',3);
insert into Plasters values (null,'Bacon','Bacon.jpg',3);
insert into Plasters values (null,'BooBoo','Booboo.jpg',6);
insert into Plasters values (null,'Comic Strips','ComicStrip.jpg',6);
insert into Plasters values (null,'CSI','CSI.jpg',6);
insert into Plasters values (null,'Cupcake','Cupcake.jpg',8);
insert into Plasters values (null,'Eyeball','Eyeball.jpg',8);
insert into Plasters values (null,'Mustache','Mustache.jpg',10);
insert into Plasters values (null,'Pickle','Pickle.jpg',10);
insert into Plasters values (null,'Playingcards','Playingcards.jpg',12);
insert into Plasters values (null,'Scabs','Scabs.jpg',12);
insert into Plasters values (null,'Toast','Toast.jpg',12);
insert into Plasters values (null,'Underpants','Underpants.jpg',12);


DROP TABLE IF EXISTS Plaster_Order;

CREATE TABLE Plaster_Order(
	id INT auto_increment,
    Order_Ref  int ,
	Customer_id varchar(255),
    Plaster_id  varchar(255),
    Status varchar(20), 
    delivery_type varchar(20),
    Paid boolean, 
    PRIMARY KEY (`id`)
);

insert into Plaster_Order values (null,1,"Joe","BooBoo",'Ordered','Delivery',true);
insert into Plaster_Order values (null,2,"Joe","Cupcake",'Ordered','Collection',false);
insert into Plaster_Order values (null,3,"Tom",'Playingcards','Ordered','Delivery',true);
insert into Plaster_Order values (null,4,"Joe","Toast",'Ordered','Collection',true);
insert into Plaster_Order values (null,5,"Tom",'Playingcards','Processing','Delivery',true);
insert into Plaster_Order values (null,6,"Joe","Toast",'Processed','Collection',true);
insert into Plaster_Order values (null,6,"Joe","Toast",'Ordered','Collection',true);
insert into Plaster_Order values (null,7,"Joe","BooBoo",'Ordered','Delivery',true);
insert into Plaster_Order values (null,8,"Joe","Cupcake",'Ordered','Collection',false);
insert into Plaster_Order values (null,9,"Joe",'Playingcards','Ordered','Delivery',true);
insert into Plaster_Order values (null,10,"Joe","Toast",'Ordered','Collection',true);
insert into Plaster_Order values (null,11,"Tom",'Playingcards','Processing','Delivery',true);
insert into Plaster_Order values (null,12,"Joe","Toast",'Processing','Collection',true);
insert into Plaster_Order values (null,13,"Joe","Toast",'Ordered','Collection',true);

select * from Plasters;
select * from user;
select * from Plaster_Order;
