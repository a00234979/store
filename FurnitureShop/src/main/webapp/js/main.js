var userURL = "http://localhost:8080/FurnitureShop/rest";

var users;

var findUser = function() {
	console.log("finduser");
	var username = $("#username").val();

	$.ajax({
		type : 'GET',
		url : userURL + "/users/usersearch/" + username,
		dataType : "json", // data type of response
		success : display_user
	});
};

var display_user = function(userList) {

	$.each(userList, function(index, user) {
		if (user.password == $("#password").val()) {
			if (user.user_type == "Picker") {
				$('#username').text(user.username);
				PickerNavBar();
				pickerList();				
				
			} else if (user.user_type == "Customer") {
				$('#username').text(user.username);
				CustomerNavBar();
				ordersList();
				ItemList();
			}
		} else {
			$('#name').text("Invalid information entered");
		}
	});
};


//Picker nav bar
var PickerNavBar = function() {
	$('.mainArea').empty();
	$('.mainArea').append('<div class="mainAreaNav">');
	$('.mainArea .mainAreaNav')
			.append(
					'<ul class="nav nav-pills" role="tablist">'
							+ '<li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#CusOrders">Customer Orders</a></li>'
							+ '</ul>'
							+ '<div class="tab-content">'
							+ '<div id="CusOrders" class="container tab-pane active"><br>'
							+ '<h3 id="Pickers">Pickers</h3>'
							+ '<p>Customers Orders List</p>'
							+ '<div class="order_list">' + '</div>' + '</div>'
							+ '</div>');
}

var pickerList = function() {
	$.ajax({
		type : 'GET',
		url : userURL + "/plaster_order",
		dataType : "json", // data type of response
		success : RenderPickerList
	});
};

var RenderPickerList = function(pickerLists) {
	$('.order_list').empty();
	$('.order_list').append("<h1> </h1>");
	$('.order_list')
			.append(				
					'<table id="pickerList" class="table table-bordered table-hover" cellspacing="0" width="100%">'
							+ '<thead> '
							+ '<th>Order Ref</th>'
							+ '<th>Customer</th>'
							+ '<th>Plaster</th>'
							+ '<th>Status</th>'
							+ '<th>Paid</th>'
							+ '<th>Dispatch Type</th>'
							+ '<th>Edit</th>'
							+ '</thead><tbody class = "contents">');
   
	$.each(pickerLists, function(index, order) {

		 var displayPaid = "No";		 
			if (order.paid == true){
				displayPaid = "Yes"
			} else {
				displayPaid = "No"
			}
		
		
		$('.contents').append(
				'<tr>' + '<td>' + order.id + '</td>' + 
				'<td>' + order.customer_id + '</td>' + 
				'<td>'  + order.plaster_id + '</td>' + 
				'<td>' + order.status
						+ '</td>' + '<td>' +displayPaid  + '</td>'
						+ '<td>' + order.delivery_type + '</td>'
						+ '<td><button type="button"  id="CusOrder'+order.id+'" data-toggle="modal"  data-target="#PickerModal" onclick="PickAnOrder(\''+ order.id +'\',\''+ order.plaster_id +'\',\''+ order.status +'\',\''+ order.customer_id +'\')" class="BuyButton"><span class="glyphicon glyphicon-edit"></span></button></td>'
						+ '</tr>');
			}		
	);  

	$('.contents').append('</tbody> </table>');
	$('#pickerList').DataTable();
	
	$('.order_list')
	.append(

			'<div id="PickerModal" class="modal fade" role="dialog">'
					+ '<div class="modal-dialog>'
					+ '<!-- Modal content-->'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">&times;</button>'
					+ '<h4 id = "ModalCusOrder" class="modal-title">Order Details</h4>'
					+ '</div>'
					+ '<div class="modal-body-amend">'
					+ '<p>Some text in the modal.</p>'
					+ '</div>'
					+ '<div class="modal-footer">'	
					+ '<div class="modal-footer-amend">'	
					+ '<button type="button" id ="PickerAlterClose" class="BuyButton" data-dismiss="modal">Close</button>'
					+ '</div></div></div></div></div>');
	
};


var PickAnOrder = function(id,name,status,customer) {
	$('.modal-body-amend').empty();
	$('.modal-body-amend').append(  
	'<h2> Customers Order <h2> '+'<br> Plaster Name:'+ name+'<br> Customer : '+customer+'<br> Status '+status+'</h2>'
	);
	
	
	if (status == "Ordered"){
		$('.modal-body-amend').append(  
				'<button type="button" id="alterprocess" class="BuyButton" onclick="ProcessOrder(\''+ id +'\')" data-dismiss="modal">Processing</button>'
				);
	} else if ( status == "Processing"){
	$('.modal-body-amend').append(  
			'<button type="button" id="alterprocess" class="BuyButton" onclick="ProcessOrder(\''+ id +'\')" data-dismiss="modal">Processed</button>'
			);
	}
}


/*
 * alter the  status of the order in the database 
 */



var ProcessOrder = function (id) {
	console.log('Process order status');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: userURL+'/plaster_order/process'+'/'+id,
		success: function(textStatus, jqXHR){
			alert('The status of the Order has been updated.');
			pickerList();	
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('You can no longer alter the status of this order.');
		}
	});		
};


// Customer nav bar

var CustomerNavBar = function() {
	$('.mainArea').empty();
	$('.mainArea').append('<div class="mainAreaNav">');
	$('.mainArea .mainAreaNav')
			.append(
					'<ul class="nav nav-pills" role="tablist">'
							+ '<li class="nav-item"><a class="nav-link active" data-toggle="pill" id="shoppingTab" href="#shopping">Shopping</a></li>'
							+ '<li class="nav-item"><a class="nav-link" data-toggle="pill" id="orderingTab" href="#orders">Orders</a></li>'
							+ '</ul>'
							+ '<div class="tab-content">'
							+ '<div id="shopping" class="container tab-pane active"><br>'
							+ '<h3 id="ShoppingTitle">Shopping</h3>'
							+ '<p>All Items Ordered will be ready for collection within 24hrs from ordering.</p>'
							+ '<div class="row">'
							+ '</div>'
							+ '</div>'
							+ '<div id="orders" class="container tab-pane fade"><br>'
							+ '<h3 id="OrderingTitle">Orders</h3>'
							+ '<p>Your current order list. </p>'
							+ '<div class="order_list">' + '</div>' + '</div>'
							+ '</div>');
}

// shopping list

var ItemList = function() {
	$.ajax({
		type : 'GET',
		url : userURL + "/plasters",
		dataType : "json", // data type of response
		success : RenderItemList
	});
};

var RenderItemList = function(itemLists) {
	$.each(itemLists,
			function(index, item) {
				$('.row').append(
						'<div class="col-sm-6 col-md-4 col-lg-3">'
								+ '<div class="products">' + '<img src="pics/' + item.image + '" height="220" width="220">'
								+ '<center><h2">' + item.name + '</h2></center>'
								+ '<center><h2> &euro; ' + item.price + '</h2></center>'
								+ '<a href="#" id ="Item'+item.id+'" data-toggle="modal"  data-target="#BuyModal" onclick="placeAnOrder(\''+ item.id +'\',\''+ item.name +'\')" class="BuyButton">BUY</a>'
								+ '</div>' 
								+ '</div>');	
			});

	$('#shopping')
			.append(

					'<div id="BuyModal" class="modal fade" role="dialog">'
							+ '<div class="modal-dialog">'
							+ '<!-- Modal content-->'
							+ '<div class="modal-content">'
							+ '<div class="modal-header">'
							+ '<button type="button" class="close" data-dismiss="modal">&times;</button>'
							+ '<h4 id="modalTitle" class="modal-title">Order Confirmation</h4>'
							+ '</div>'
							+ '<div class="modal-body">'
							+ '<p>Some text in the modal.</p>'
							+ '</div>'
							+ '<div class="modal-footer">'	
							+ '<button type="button" id="Cancel" class="BuyButton" data-dismiss="modal">Cancel</button>'
							+ '</div>' + '</div>' + '</div>' + '</div>');
};

/*
 * displays  the order lists 
 * 
 */
var ordersList = function() {
	$.ajax({
		type : 'GET',
		url : userURL + "/plaster_order",
		dataType : "json", // data type of response
		success : RenderOrdersList
	});
};

var RenderOrdersList = function(orderLists) {
	$('.order_list').empty();
	$('.order_list').append("<h1> Orders status </h1>");

	$('.order_list')
			.append(				
					'<table id="dtBasicExample"  class="table table-bordered table-hover" cellspacing="0" width="100%">'
							+ '<thead> '
							+ '<th>Order Ref</th>'
							+ '<th>Plaster Type</th>'
							+ '<th>Status</th>'
							+ '<th>Paid</th>'
							+ '<th>Dispatch Type</th>'
							+ '<th>Edit</th>'
							+ '</thead><tbody class = "contents">');

	$.each(orderLists, function(index, order) {
		if (order.customer_id == $('#username').val()){
		 var sizeProgress = 0; 

		 var colourSetting = "progress-bar progress-bar-striped"
		if (order.status == "Ordered"){
			sizeProgress = 33; 
			colourSetting = "progress-bar progress-bar-striped progress-bar-warning"
		} else if (order.status == "Processing"){
			sizeProgress = 66; 
			colourSetting = "progress-bar progress-bar-striped progress-bar-info"
		}	else if (order.status == "Processed"){
			sizeProgress = 100; 
			colourSetting = "progress-bar progress-bar-striped progress-bar-success active"
		}
		 
		 var displayPaid = "No";		 
		if (order.paid == true){
			displayPaid = "Yes"
		} else {
			displayPaid = "No"
		}
		 
		 
			
		$('.contents').append(
				'<tr>' + '<td>' + order.id + '</td>' + '<td>'
						+ order.plaster_id + '</td>' + '<td>' + order.status
						+'<div class= "progress">'
						+'<div class="'+colourSetting+'" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: '+sizeProgress+'%"></div>'
						+'</div>'						
						+'</td>' + '<td>' + displayPaid 
						+'</td>'
						+ '<td>' + order.delivery_type + '</td>'
						+ '<td><button type="button" data-toggle="modal"  data-target="#AlterModal" id="Order'+order.id+'" onclick="AlterAnOrder (\''+ order.id +'\',\''+ order.plaster_id +'\')" class="BuyButton"><span class="glyphicon glyphicon-edit"></span></button></td>'
						+ '</tr>');
			}
		}
	);  
	$('.contents').append('</tbody> </table>');
	$('#dtBasicExample').DataTable();
	
	$('.order_list')
	.append(

			'<div id="AlterModal" class="modal fade" role="dialog">'
					+ '<div class="modal-dialog>'
					+ '<!-- Modal content-->'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">&times;</button>'
					+ '<h4 id="ModalAlterOrder" class="modal-title">Order Details</h4>'
					+ '</div>'
					+ '<div class="modal-body-amend">'
					+ '<p>Some text in the modal.</p>'
					+ '</div>'
					+ '<div class="modal-footer">'	
					+ '<div class="modal-footer-amend">'	
					+ '<button type="button" class="BuyButton" data-dismiss="modal">Close</button>'
					+ '</div></div></div></div></div>');
};


var AlterAnOrder = function(id,name) {
	$('.modal-body-amend').empty();
	$('.modal-body-amend').append(  
	'<h2> you have ordered the plaster design: </p>'+
	' '+ name+'</h2>'+
	'<h3> Dispatch options </h3>'+
	'<input type="radio" id="Collection" name="DispatchType" value="Collection">'+
	'<label for="Colection">Collection </label>'+
	'<input type="radio" id="Delivery" name="DispatchType" value="Delivery" checked="checked">'+
	'<label for="Delivery"> Delivery </label><br><br>'+ 
	'<button type="button" class="BuyButton" onclick="AlterOrder(\''+ name +'\',true,\''+ id +'\')" data-dismiss="modal">Confirm & Pay</button>'+
	'<button id ="CancelOrder" type="button" class="BuyButton" onclick="DeleteOrder(\''+ id +'\')" data-dismiss="modal">Cancel Order</button></p>'	
	);
}


/*
 * alter the order to the database 
 */

var AlterOrder = function (name,paid,id) {
	console.log('addOrder');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: userURL+'/plaster_order'+'/'+id,
		dataType: "json",
		data: JSON.stringify({
					"id": id, 
					"order_ref": 1, 
					"customer_id": $('#username').val(),
					"plaster_id": name,
					"status": "Ordered",
					"delivery_type":  document.querySelector('input[name="DispatchType"]:checked').value,
					"paid": paid
					}),
		success: function(data, textStatus, jqXHR){
			alert('You have updated your order.');
			ordersList();	
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('You can no longer alter this order');
		}
	});		
};


var DeleteOrder = function (id) {
	console.log('Delete Order');
	$.ajax({
		type: 'DELETE',
		url: userURL+'/plaster_order'+'/'+id,
		success: function(data, textStatus, jqXHR){
			alert('Your order has been deleted');
			ordersList();	
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Cutomer Notice , You can no longer alter this order');
		}
	});		
};


/*
 * displays the modal to confirm order  
 */

var placeAnOrder = function(id,name) {
	$('.modal-body').empty();
	$('.modal-body').append(  
	'<h2> you have ordered the plaster design: </p>'+
	' '+ name+'</h2>'+
	'<h3> Dispatch options </h3>'+
	'<input type="radio" id="Collection" name="DispatchType" value="Collection">'+
	'<label for="Colection">Collection </label>'+
	'<input type="radio" id="Delivery" name="DispatchType" value="Delivery" checked="checked">'+
	'<label for="Delivery"> Delivery </label><br>'+ 
	'<p> <button type="button" class="BuyButton" id="addOrder1" onclick="addOrder(\''+ name +'\',false)" data-dismiss="modal">Confirm</button> Or '+
	'<button type="button" class="BuyButton" id="addOrder2" onclick="addOrder(\''+ name +'\',true)" data-dismiss="modal">Confirm & Pay</button></p>'
	);
}


/*
 * Add the order to the database 
 */

var addOrder = function (name,paid) {
	console.log('addOrder');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: userURL+'/plaster_order',
		dataType: "json",
		data: JSON.stringify({
					"id": null, 
					"order_ref": 1, 
					"customer_id": $('#username').val(),
					"plaster_id": name,
					"status": "Ordered",
					"delivery_type":  document.querySelector('input[name="DispatchType"]:checked').value,
					"paid": paid
					}),
		success: function(data, textStatus, jqXHR){
			alert('Thank you for ordering. ');
			ordersList();	
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('order error: ' + textStatus);
		}
	});		
};


var logout = function() {
	$('.mainArea').empty();
	$('.mainArea').append("<h1>Thank you for using PLASTERSRESTS</h1>");
	document.getElementById("username").value = "";
	document.getElementById("password").value = "";
}

// When the DOM is ready.
$(document).ready(function() {
	$('#submitBTN').click(function() {
		findUser();
	});
	$('#LogoutBTN').click(function() {
		logout();
	});	
});
