package com.FurnitureShop.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.FurnitureShop.model.User;


@Stateless
@LocalBean
public class UserDAO {

    @PersistenceContext
    private EntityManager em;
    
	public List<User> getAllUsers() {
    	Query query=em.createQuery("SELECT u FROM User u");
        return query.getResultList();
    }
	
	
	public User getUser(int id) {		
		return em.find(User.class, id); 
	}
	
	public void save(User user) {
		em.persist(user);
	}
	

	public List<User> getUserByExactName(String name) {
		Query query = em.createQuery("Select u from User u Where u.username Like ?1");
		query.setParameter(1, name);
		return query.getResultList(); 
	}


}
