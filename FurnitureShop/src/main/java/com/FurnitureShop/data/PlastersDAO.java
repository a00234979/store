package com.FurnitureShop.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.FurnitureShop.model.Plasters;


@Stateless
@LocalBean
public class PlastersDAO {

    @PersistenceContext
    private EntityManager em;
    
	public List<Plasters> getAllPlasters() {
    	Query query=em.createQuery("SELECT p FROM Plasters p");
        return query.getResultList();
    }
	
	public Plasters getPlasters(int id) {	
		return em.find(Plasters.class, id); 
	}
	
	
	public void save(Plasters plasters) {
		em.persist(plasters);
	}
	

}
