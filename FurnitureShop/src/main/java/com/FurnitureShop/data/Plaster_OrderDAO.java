package com.FurnitureShop.data;

import java.sql.ResultSet;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.PathParam;

import com.FurnitureShop.model.Plaster_Order;



@Stateless
@LocalBean
public class Plaster_OrderDAO {

    @PersistenceContext
    private EntityManager em;
    
	public List<Plaster_Order> getAllPlasters_Order() {
    	Query query=em.createQuery("SELECT p FROM Plaster_Order p");
        return query.getResultList();
    }
	
	public Plaster_Order getPlaster_Order(int id) {	
		return em.find(Plaster_Order.class, id); 
	}
	
	
	public void save(Plaster_Order plaster_Order) {
		em.persist(plaster_Order);
	}
	
	public void update(Plaster_Order plaster_Order) {
		em.merge(plaster_Order); 
	}
	
	public int update(Plaster_Order plaster_Order, int id) {
		Query query=em.createQuery("SELECT p FROM Plaster_Order p Where id=?1 and status='Ordered' ");
		query.setParameter(1, id);	
		try {
			System.out.println(query.getSingleResult().toString());
		} catch (Exception e) {
			return 402;
		}	
		em.merge(plaster_Order); 
		return 200; 
	}
	
	
	public int delete(int id) {
		Query query=em.createQuery("SELECT p FROM Plaster_Order p Where id=?1 and status='Ordered' ");
		query.setParameter(1, id);	
		try {
			System.out.println(query.getSingleResult().toString());
		} catch (Exception e) {
			return 409;
		}	
		em.remove(getPlaster_Order(id));
		return 204; 
	}

	public int processPlasters(int id) {		
		Query query=em.createQuery("SELECT p FROM Plaster_Order p Where id=?1 and status = 'Processing' ");
		query.setParameter(1, id);
		try {
			System.out.println(query.getSingleResult().toString());
			query = em.createQuery("Update Plaster_Order Set status ='Processed' where  id=?1 ");
		    query.setParameter(1, id).executeUpdate();  
		    return 200; 
		} catch (Exception e) {			
			query=em.createQuery("SELECT p FROM Plaster_Order p Where id=?1 and paid=true ");
			query.setParameter(1, id);		
			try {
				System.out.println(query.getSingleResult().toString());
				query = em.createQuery("Update Plaster_Order Set status = 'Processing' where  id=?1 ");
			    query.setParameter(1, id).executeUpdate();   
			} catch (Exception e1) {	
				return 418;
			}
		}		  
		return 200; 
	}	

}
