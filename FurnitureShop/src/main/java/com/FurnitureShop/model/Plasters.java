package com.FurnitureShop.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@XmlRootElement


public class Plasters {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String name;
	private String image;
	private int price;
	
	public int getId() {
		return id;
	}
	 
	public String getName() {
		return name;
	}
	
	public String getImage() {
		return image;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Plasters [id=" + id + ", name=" + name + ", image=" + image + ", price=" + price + "]";
	}
}