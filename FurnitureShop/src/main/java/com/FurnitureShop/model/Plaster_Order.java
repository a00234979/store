package com.FurnitureShop.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@XmlRootElement


public class Plaster_Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private int order_ref;
	private String customer_id;
	private String plaster_id;
	private String status; 
	private String delivery_type; 
	private boolean paid;
	
	
	
	public String getDelivery_type() {
		return delivery_type;
	}
	public void setDelivery_type(String delivery_type) {
		this.delivery_type = delivery_type;
	}
	public int getId() {
		return id;
	}
	public int getOrder_ref() {
		return order_ref;
	}
	
	public String getCustomer_id() {
		return customer_id;
	}
	
	public String getPlaster_id() {
		return plaster_id;
	}
	
	
	public String getStatus() {
		return status;
	}
	
	public boolean isPaid() {
		return paid;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setOrder_ref(int order_ref) {
		this.order_ref = order_ref;
	}
	
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	
	public void setPlaster_id(String plaster_id) {
		this.plaster_id = plaster_id;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setPaid(boolean paid) {
		this.paid = paid;
	}
	
	@Override
	public String toString() {
		return "Plaster_order [id=" + id + ", order_ref=" + order_ref + ", customer_id=" + customer_id + ", plaster_id="
				+ plaster_id + ", status=" + status + ", paid=" + paid + "]";
	} 
	
	
} 