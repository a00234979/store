package com.FurnitureShop.rest;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.FurnitureShop.data.UserDAO;
import com.FurnitureShop.model.User;


@Path("/users")
@Stateless
@LocalBean
public class UserWS {

	@EJB
	private UserDAO userDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllUsers() {
		System.out.println("Get all users++test");
		List<User> users=userDao.getAllUsers();
		System.out.println("got users");
		System.out.println(users.size());
		return Response.status(200).entity(users).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findUserByID(@PathParam("id") int id) {
		User  user = userDao.getUser(id);
			return Response.status(200).entity(user).build(); 
	}
	

	
	@GET @Path("usersearch/{query}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<User> findByExactName(@PathParam("query") String query){
		System.out.println("find By Exact Name: " +query);

		return userDao.getUserByExactName(query);
	}

}
