package com.FurnitureShop.rest;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.FurnitureShop.data.Plaster_OrderDAO;
import com.FurnitureShop.model.Plaster_Order;




@Path("/plaster_order")
@Stateless
@LocalBean
public class Plaster_OrderWS {

	@EJB
	private Plaster_OrderDAO plaster_orderDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllPlaster_Order() {
		System.out.println("Get all Plaster_Order++test");
		List<Plaster_Order> plaster_order=plaster_orderDao.getAllPlasters_Order();
		System.out.println("got plasters_orders");
		System.out.println(plaster_order.size());
		return Response.status(200).entity(plaster_order).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findPlastersByID(@PathParam("id") int id) {
		Plaster_Order plaster_order = plaster_orderDao.getPlaster_Order(id);
			return Response.status(200).entity(plaster_order).build(); 
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON})
	public Response savePlasterOrder(Plaster_Order plaster_order) {
		plaster_orderDao.save(plaster_order);	
		return Response.status(201).entity(plaster_order).build();	
	}
	
	
	
	
	@PUT
	@Path("/{id}")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON})
	public Response updatePlasters(Plaster_Order plaster_order,@PathParam("id") int id) {	
		int valueReturn = plaster_orderDao.update(plaster_order,id); 
		return Response.status(valueReturn).entity(plaster_order).build(); 
	}

	
	@PUT
	@Path("/process/{id}")
	public Response processPlasters(Plaster_Order plaster_order,@PathParam("id") int id) {	
		int valueReturn = plaster_orderDao.processPlasters(id); 
		return Response.status(valueReturn).build(); 
	}
		
	@DELETE
	@Path("/{id}")
	public Response deletePlasters(@PathParam("id") int id) {
		int valueReturn = plaster_orderDao.delete(id); 
		return Response.status(valueReturn).build(); 	
	}
		

}
