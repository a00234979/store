package com.FurnitureShop.rest;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.FurnitureShop.data.PlastersDAO;
import com.FurnitureShop.model.Plasters;



@Path("/plasters")
@Stateless
@LocalBean
public class PlastersWS {

	@EJB
	private PlastersDAO plastersDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllPlasters(){
		System.out.println("Get all plasters++test");  
		List<Plasters> plasters=plastersDao.getAllPlasters();
		System.out.println("got plasters");
		System.out.println(plasters.size());   
		return Response.status(200).entity(plasters).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findPlastersByID(@PathParam("id") int id) {
		Plasters plasters = plastersDao.getPlasters(id);
			return Response.status(200).entity(plasters).build(); 
	}
}
