@browser
Feature: The Login Page
	As a registered user
	I want to login to the application 
	So that I can use according to my rights 

	
	Background:
	Given the Picker is logged in 
	
	
	Scenario: The Picker can select an order to view
		When The picker select the edit button for a customer order
		Then The order will be displayed 
		
	Scenario: The Picker can close the window 
		When The picker select the edit button for a customer order
		Then The order will be displayed 		
		Then The picker select the close button	
		Then The Customer orderLists will be displayed 
		
  	Scenario: The Picker can upgrade an order
		When The picker select the edit button for a customer order
		Then The order will be displayed 		
		Then The picker clicks on Processed	
		Then The picker will see an alert stating order has been updated 		