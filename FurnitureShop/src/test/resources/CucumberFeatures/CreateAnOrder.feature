@browser
Feature: place order with options for payment and dischare options (delivery/collection) 
	As a registered user located
	on Order Confirmation 
	So that they can order the Item 
	
	Background:
	Given the Customer is logged in their account.
	And is on Order Confirmation Modal
	
	Scenario: The user should be able pay and select Delivery , place the order 
		When The user selects Delivery
		Then The user select confirm and pay to place the order 
	  Then The user clicks the pop up window when the order placed 
	
	Scenario: The user should be able not pay and select Collection, place the order 
		When The user selects Collection
		Then The user selects confirm to place the order 
		Then The user clicks the pop up window when the order placed 
		
		Scenario: The user should be able to cancel create an order  
		When The user selects cancel
		Then The user is brought back to the shopping page