@browser
Feature: View the Ordering list 
	As a registered user
	I want to view the ordering list
	So that i can view/alter it 
	
	Background:
	Given the Customer is logged in to their personal account
	And is in the customer order tabs
	
	
	Scenario: The user should be able to view the order  
		When The user clicks on to the edit button 
		Then The user sees the order
	
		Scenario: The user should not be able to cancel the order
		When The user clicks on to the edit button for an order
		And The user clicks cancelOrder
		Then The user sees the pop up stating they can not alter order 

		Scenario: The user should able to cancel the order
		When The user clicks on to the edit button for an order to be removed
		And The user clicks cancelOrder
		Then The user sees the pop up stating they can delete