@browser
Feature: View the shopping list 
	As a registered user
	I want to view the shopping list 
	So that I select 
	
	Background:
	Given the Customer is logged in 
	
	
	Scenario: The user should be able to see the Shopping list 
		When The user selects the Shopping tab 
		Then The user should be able to see the list of items
	
	
