@browser
Feature: Selecting an Item to Order  
	As a registered user located
	on the shopping list 
	So that I can select an item to order
	
	Background:
	Given the Customer is logged in their account
	And is on the shopping tab 
	
	
	Scenario: The user should be able to see the Shopping list 
		When The user presses the buy button of an Item in the Shopping List 
		Then The user should see the a screen displaying items details
	
	
