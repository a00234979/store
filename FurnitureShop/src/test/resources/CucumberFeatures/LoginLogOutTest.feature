@browser
Feature: The Login Page
	As a registered user
	I want to login to the application 
	So that I can use according to my rights 

	
	Background:
	Given the user is on the login page
	
	
	Scenario: The user shouldn't be able to login
		When the user enters valid Joe invalid password password
		Then the user should not be able to log in giving Invalid information entered
	
	Scenario: The customer should be able to login
		When the user enters valid Joe valid password password
		Then the user should get a valid message 

	Scenario: The customer should be able to logout
		When the user enters valid Joe valid password password
		Then the user should get a valid message 
		When the user clicks on logout
		Then the user should get a valid message saying they logged out
		
		
		
	Scenario: The user should be able to logout
		When the user enters valid Bridget valid password password
		Then the user should get a valid screen 
		When the user clicks on logout
		Then the user should get a valid message saying they logged out