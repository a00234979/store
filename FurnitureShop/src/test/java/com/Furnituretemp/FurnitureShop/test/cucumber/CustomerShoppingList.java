package com.Furnituretemp.FurnitureShop.test.cucumber;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CustomerShoppingList  {
	
	private WebDriver driver = Hooks.driver;
	

	@Given("^the Customer is logged in to their account$")
	public void loginScreen() {
		driver.get("http://localhost:8080/FurnitureShop/index.html"); 	
		driver.findElement(By.id("username")).sendKeys("Joe");   
		driver.findElement(By.id("password")).sendKeys("password");
		driver.findElement(By.id("submitBTN")).click(); 

	}
	
	@Then("^The user selects the Orders tab$")
	public void TheUserSelectsTheOrderingTab () throws InterruptedException {
		
	driver.findElement(By.id("orderingTab")).click(); 
	Thread.sleep(1000);
	} 
	
	@Then("^The user should be able to see the list of orders$")
	public void TheUserShouldBeAbleToSeeTheListOfItems() {	
		
		WebElement statusMessage = driver.findElement(By.id("OrderingTitle")); 
		assertEquals(statusMessage.getText(),"Orders");
	}		

}
