package com.Furnituretemp.FurnitureShop.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.FurnitureShop.data.PlastersDAO;
import com.FurnitureShop.model.Plasters;
import com.FurnitureShop.rest.JaxRsActivator;
import com.FurnitureShop.rest.PlastersWS;
import com.Furnituretemp.FurnitureShop.test.utils.UtilsDAO;



		@RunWith(Arquillian.class)
		public class PlastersDAOWSTest {
			
			@Deployment
			public static Archive<?> createTestArchive() {
				return ShrinkWrap
						.create(JavaArchive.class, "Test.jar")
						.addClasses(PlastersDAO.class, Plasters.class,
								JaxRsActivator.class,PlastersWS.class,
								UtilsDAO.class)
						.addAsManifestResource("META-INF/persistence.xml",
								"persistence.xml")
						.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

			}

			 
			@EJB 
			private PlastersWS plastersWS;
			
			@EJB
			private PlastersDAO plastersDAO;
			
			@EJB
			private UtilsDAO utilsDAO;
			
			 
			@Before
			public void setUp() {

				utilsDAO.deletePlastersDataTable();
				
				Plasters plasters= new Plasters();
				plasters.setName("Abraham");
				plasters.setImage("Abraham.jpg");
				plasters.setPrice(3);
				plastersDAO.save(plasters);
	
				Plasters plasters1= new Plasters();
				plasters1.setName("Bacon");
				plasters1.setImage("Bacon.jpg");
				plasters1.setPrice(3);
				plastersDAO.save(plasters1);
		
			}
			
			@Test
			public void testGetAllPlasters() {
				List<Plasters> plastersList = plastersDAO.getAllPlasters();
				assertEquals("Data fetch = data persisted", plastersList.size(), 2);
				assertEquals(plastersWS.findAllPlasters().getStatus(), 200);
			}		

			@Test
			public void testGetPlastersByNameById() {
				Plasters plasters = plastersDAO.getPlasters(1);				
				assertEquals(plasters.getName(),"Abraham");              
				assertEquals(plasters.getImage(),"Abraham.jpg");
				assertEquals(plasters.getPrice(),3);				
				assertEquals(plastersWS.findPlastersByID(1).getStatus(), 200);
			}		
			
			
			
}
