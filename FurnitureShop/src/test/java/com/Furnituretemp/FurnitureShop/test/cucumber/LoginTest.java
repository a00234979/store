package com.Furnituretemp.FurnitureShop.test.cucumber;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginTest  {
	
	private WebDriver driver = Hooks.driver;
	

	@Given("^the user is on the login page$")
	public void loginScreen() {
		driver.get("http://localhost:8080/FurnitureShop/index.html"); 	
	}

	
	@When("^the user enters valid Joe invalid password password$")
	public void enterInUserAndPasswordInvalid() throws InterruptedException {
		driver.findElement(By.id("username")).sendKeys("Joe");   
		driver.findElement(By.id("password")).sendKeys("password1");
		driver.findElement(By.id("submitBTN")).click(); 
		Thread.sleep(1000);
	}
	
	
	@Then("^the user should not be able to log in giving Invalid information entered$")
	public void enterInUserAndPasswordreply() {
		WebElement statusMessage = driver.findElement(By.id("name")); 
		assertEquals(statusMessage.getText(),"Invalid information entered");
	}	
	
	
	@When("^the user enters valid Joe valid password password$")
	public void enterInUserAndPasswordValid() {
		driver.findElement(By.id("username")).sendKeys("Joe");   
		driver.findElement(By.id("password")).sendKeys("password");
		driver.findElement(By.id("submitBTN")).click(); 
	}
	

	@Then("^the user should get a valid message$")
	public void enterInUserAndPasswordreplyvalid() {
		WebElement statusMessage = driver.findElement(By.id("username")); 
		assertEquals(statusMessage.getText(),"Joe");
	}	
	

	
	@When("^the user clicks on logout$")
	public void UserLogoutbuttonactivation() {
		driver.findElement(By.id("LogoutBTN")).click(); 
	}
	

	@Then("^the user should get a valid message saying they logged out$")
	public void UserLogoutMessage() {
		WebElement statusMessage = driver.findElement(By.id("name")); 
		assertEquals(statusMessage.getText(),"Thank you for using PLASTERSRESTS");
	}	
	
	@When("^the user enters valid Bridget valid password password$")
	public void theUserEntersValidBridgetValidPasswordpassword() {
		driver.findElement(By.id("username")).sendKeys("Bridget");   
		driver.findElement(By.id("password")).sendKeys("password");
		driver.findElement(By.id("submitBTN")).click(); 
	}
	
	
	@Then("^the user should get a valid screen$")
	public void theUserShouldGetAValidScreen() {
		WebElement statusMessage = driver.findElement(By.id("username")); 
		assertEquals(statusMessage.getText(),"Bridget");
	}
	
	
	
	
	
	
	
	
	


}
