package com.Furnituretemp.FurnitureShop.test.cucumber;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.*;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateAnOrder  {
	
	private WebDriver driver = Hooks.driver;
	

	@Given("^the Customer is logged in their account.$")
	public void loginScreen() {
		driver.get("http://localhost:8080/FurnitureShop/index.html"); 	
		driver.findElement(By.id("username")).sendKeys("Joe");   
		driver.findElement(By.id("password")).sendKeys("password");
		driver.findElement(By.id("submitBTN")).click(); 

	}
	
	@And("^is on Order Confirmation Modal$")
	public void isOnTheShoppingModal() throws InterruptedException{
		Thread.sleep(1000);	
	driver.findElement(By.id("shoppingTab")).click(); 	
	driver.findElement(By.id("Item1")).click();
	}		

	@When("^The user selects Delivery$")
	public void TheUserSelectsDelivery() throws InterruptedException {	
		Thread.sleep(1000);
		driver.findElement(By.id("Delivery")).click();
	}		
	
	@Then("^The user select confirm and pay to place the order$")
	public void TheUserSelectConfirmAndPayToPlaceTheOrder() throws InterruptedException {	
		Thread.sleep(1000);
		driver.findElement(By.id("addOrder2")).click();
		
		
	}
	
	
	@When("^The user selects Collection$")
	public void TheUserSelectsCollection() throws InterruptedException {	
		Thread.sleep(1000);
		driver.findElement(By.id("Collection")).click();
	}		
	
	@Then("^The user selects confirm to place the order$")
	public void TheUserSelectsConfirmToPlaceTheOrder() throws InterruptedException {	
		Thread.sleep(1000);
		driver.findElement(By.id("addOrder1")).click();
	}
		
	@Then("^The user clicks the pop up window when the order placed$")
	public void TheUserClicksThePopUpWindowWhenTheOrderPlaced() throws InterruptedException {	
	Thread.sleep(3000);
	Alert alert = driver.switchTo().alert();
	String AlertMessage = driver.switchTo().alert().getText();
	System.out.print("****"+AlertMessage+"****");
	assertEquals(AlertMessage,"Thank you for ordering. " );
	Thread.sleep(3000);
	alert.accept();
	}
	
	
	@When("^The user selects cancel$")
	public void TheUserSelectsCancel() throws InterruptedException {	
		Thread.sleep(1000);
		driver.findElement(By.id("Cancel")).click();
	}		
	
	@Then("^The user is brought back to the shopping page$")
	public void TheUserIsBroughtBackToTheShoppingPage() throws InterruptedException {	
		Thread.sleep(1000);
		WebElement statusMessage = driver.findElement(By.id("ShoppingTitle")); 
		assertEquals(statusMessage.getText(),"Shopping");
	}
	
	
	
	
	
	
	
}

