package com.Furnituretemp.FurnitureShop.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.PathParam;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.FurnitureShop.data.Plaster_OrderDAO;
import com.FurnitureShop.model.Plaster_Order;
import com.FurnitureShop.rest.JaxRsActivator;
import com.FurnitureShop.rest.Plaster_OrderWS;
import com.Furnituretemp.FurnitureShop.test.utils.UtilsDAO;



		@RunWith(Arquillian.class)
		public class Plasters_OrderDAOWSTest {
			
			@Deployment
			public static Archive<?> createTestArchive() {
				return ShrinkWrap
						.create(JavaArchive.class, "Test.jar")
						.addClasses(Plaster_OrderDAO.class, Plaster_Order.class,
								JaxRsActivator.class,Plaster_OrderWS.class,
								UtilsDAO.class)
						.addAsManifestResource("META-INF/persistence.xml",
								"persistence.xml")
						.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
			}

			 
			@EJB 
			private Plaster_OrderWS plaster_orderWS;
			
			@EJB
			private Plaster_OrderDAO plasters_orderDAO;
			
			@EJB
			private UtilsDAO utilsDAO;
			
			 
			@Before
			public void setUp() {

				utilsDAO.deletePlasters_orderTable();
				
				Plaster_Order plaster_orders= new Plaster_Order();
				plaster_orders.setOrder_ref(1);
				plaster_orders.setCustomer_id("Joe");
				plaster_orders.setPlaster_id("BooBoo");
				plaster_orders.setStatus("Ordered");
				plaster_orders.setDelivery_type("Delivery");
				plaster_orders.setPaid(true);
				plasters_orderDAO.save(plaster_orders);
				
				Plaster_Order plaster_orders1= new Plaster_Order();
				
				plaster_orders1.setOrder_ref(1);
				plaster_orders1.setCustomer_id("Joe");
				plaster_orders1.setPlaster_id("Cupcake");
				plaster_orders1.setStatus("Ordered");
				plaster_orders1.setDelivery_type("Collection");
				plaster_orders1.setPaid(false); 
				plasters_orderDAO.save(plaster_orders1);			
				
				
				Plaster_Order plaster_orders2= new Plaster_Order();
				plaster_orders2.setOrder_ref(1);
				plaster_orders2.setCustomer_id("Joe");
				plaster_orders2.setPlaster_id("Cupcake");
				plaster_orders2.setStatus("Processing");
				plaster_orders2.setDelivery_type("Collection");
				plaster_orders2.setPaid(true); 
				plasters_orderDAO.save(plaster_orders2);
				
			}
			
			@Test
			public void testGetAllPlasters_Orders() {
				List<Plaster_Order> plasters_orderList = plasters_orderDAO.getAllPlasters_Order();
				assertEquals("Data fetch = data persisted", plasters_orderList.size(), 3);
				assertEquals(plaster_orderWS.findAllPlaster_Order().getStatus(), 200);
			}		

			@Test
			public void oneEqualsOne() {
				assertEquals(200, 200);
			}			
			
			
			
			
			@Test
			public void testGetPlaster_ordersByNameById() {
				Plaster_Order plasters_order = plasters_orderDAO.getPlaster_Order(1);				
				assertEquals(plasters_order.getOrder_ref(),1);           
				assertEquals(plasters_order.getCustomer_id(),"Joe");
				assertEquals(plasters_order.getPlaster_id(),"BooBoo");
				assertEquals(plasters_order.getStatus(),"Ordered");
				assertEquals(plasters_order.getDelivery_type(),"Delivery");
				assertEquals(plasters_order.isPaid(),true);
				assertEquals(plaster_orderWS.findPlastersByID(1).getStatus(), 200);
			}		
			
			
			@Test
			public void savePlasterOrder() {
				Plaster_Order plaster_order= new Plaster_Order();
				plaster_order.setOrder_ref(3);
				plaster_order.setCustomer_id("Joe");
				plaster_order.setPlaster_id("PlayingCards");
				plaster_order.setStatus("Ordered");
				plaster_order.setDelivery_type("Delivery");
				plaster_order.setPaid(true);				
				assertEquals(plaster_orderWS.savePlasterOrder(plaster_order).getStatus(), 201);
			}			
			
			
			
			@Test
			public void testUpdatePlaster_ordersByNameById() {
				Plaster_Order plasters_order = plasters_orderDAO.getPlaster_Order(1);	
				plasters_order.setStatus("processing");
				plasters_orderDAO.update(plasters_order);
				plasters_order = plasters_orderDAO.getPlaster_Order(1);
				assertEquals(plasters_order.getStatus(),"processing");
			}		
			
			@Test
			public void testUpdatePlaster_Cus_update_status_true() {
				Plaster_Order plasters_order = plasters_orderDAO.getPlaster_Order(1);	
				plasters_order.setStatus("Ordered");
				plasters_orderDAO.update(plasters_order);			
				plasters_order = plasters_orderDAO.getPlaster_Order(1);			
				assertEquals(plasters_orderDAO.update(plasters_order,1),200);
				assertEquals(plaster_orderWS.updatePlasters(plasters_order,1).getStatus(), 200);
			}	
			
			@Test
			public void testUpdatePlaster_Cus_update_status_false() {
				Plaster_Order plasters_order = plasters_orderDAO.getPlaster_Order(1);	
				plasters_order.setStatus("Processing");
				plasters_orderDAO.update(plasters_order);			
				plasters_order = plasters_orderDAO.getPlaster_Order(1);			
				assertEquals(plasters_orderDAO.update(plasters_order,1),402);
				assertEquals(plaster_orderWS.updatePlasters(plasters_order,1).getStatus(), 402);
			}		
			
			
			@Test
			public void testUpdatePlaster_Cus_delete_a_order_True() {				
				assertEquals(plasters_orderDAO.delete(1),204);
			}			
			
			
			@Test
			public void testUpdatePlaster_Cus_delete_a_order_True_status() {				
				assertEquals(plaster_orderWS.deletePlasters(1).getStatus(),204);
			}						

			@Test
			public void testUpdatePlaster_Cus_delete_a_order_False() {	

				assertEquals(plasters_orderDAO.delete(3),409);
			}			
			
			
			@Test
			public void testUpdatePlaster_Cus_delete_a_order_False_status() {				
				assertEquals(plaster_orderWS.deletePlasters(3).getStatus(),409);
			}				


			
			@Test
			public void testProcessPlaster_Picker_processPlastersPaid_False() {				
				assertEquals(plasters_orderDAO.processPlasters(2),418);
			}				
			
			@Test
			public void testProcessPlaster_Picker_processPlastersPaid_False_status() {
				Plaster_Order plasters_order = plasters_orderDAO.getPlaster_Order(2);									
				assertEquals(plaster_orderWS.processPlasters(plasters_order, 2).getStatus(),418);
			}				
	
			@Test
			public void testProcessPlaster_Picker_processPlastersProcessed_False() {		
				Plaster_Order plasters_order = plasters_orderDAO.getPlaster_Order(1);	
				plasters_order.setStatus("Processing");
				plasters_orderDAO.update(plasters_order);					
				assertEquals(plasters_orderDAO.processPlasters(1),200);
			}				
			
			@Test
			public void testProcessPlaster_Picker_processPlastersProcessed_False_status() {
				Plaster_Order plasters_order = plasters_orderDAO.getPlaster_Order(1);	
				plasters_order.setStatus("Processing");
				plasters_orderDAO.update(plasters_order);
				assertEquals(plaster_orderWS.processPlasters(plasters_order, 1).getStatus(),200);
			}	
	 
			@Test
			public void testProcessPlaster_Picker_processPlastersPaid_True_Processed() {			
				assertEquals(plasters_orderDAO.processPlasters(1),200);
				
			}				
			
			@Test
			public void testProcessPlaster_Picker_processPlastersPaid_True_processing_status() {
				Plaster_Order plasters_order = plasters_orderDAO.getPlaster_Order(1);									
				assertEquals(plaster_orderWS.processPlasters(plasters_order, 1).getStatus(),200);
			}			
							
}
