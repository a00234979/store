package com.Furnituretemp.FurnitureShop.test.cucumber;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.*;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SelectAOrderAndAlter  {
	
	private WebDriver driver = Hooks.driver;
	

	@Given("^the Customer is logged in to their personal account$")
	public void loginScreen() {
		driver.get("http://localhost:8080/FurnitureShop/index.html"); 	
		driver.findElement(By.id("username")).sendKeys("Joe");   
		driver.findElement(By.id("password")).sendKeys("password");
		driver.findElement(By.id("submitBTN")).click(); 

	}
	
	@And("^is in the customer order tabs$")
	public void TheUserSelectsTheOrderingTab () throws InterruptedException {	
		Thread.sleep(1000);
	driver.findElement(By.id("orderingTab")).click(); 
	Thread.sleep(1000);
	} 
	
	@When("^The user clicks on to the edit button$")
	public void TheUserClicksOnToTheEditButton() throws InterruptedException {	
		Thread.sleep(1000);
		driver.findElement(By.id("Order6")).click(); 	
		Thread.sleep(1000);
	}		

	@Then("^The user sees the order$")
	public void TheUserSeeTheOrder() {	
	String message = driver.findElement(By.id("ModalAlterOrder")).getText();
	assertEquals(message,"Order Details" );	
	}	
	
	
	@When("^The user clicks on to the edit button for an order$")
	public void TheUserClicksOnToTheEditButtonForAnOrder() throws InterruptedException {	
		Thread.sleep(1000);
		driver.findElement(By.id("Order6")).click(); 	
		Thread.sleep(1000);
	}
	
	@And("^The user clicks cancelOrder$")
	public void TheUserClicksCancelOrder() throws InterruptedException {	
		Thread.sleep(1000);
		driver.findElement(By.id("CancelOrder")).click(); 	
		Thread.sleep(1000);
	}	
	
	
	@Then("^The user sees the pop up stating they can not alter order$")
	public void TheUserSeesThePopUpStatingTheyCanNotAlterOrder() throws InterruptedException {	
	Thread.sleep(3000);
	Alert alert = driver.switchTo().alert();
	String AlertMessage = driver.switchTo().alert().getText();
	System.out.print("****"+AlertMessage+"****");
	assertEquals(AlertMessage,"Cutomer Notice , You can no longer alter this order" );
	Thread.sleep(2000);
	alert.accept();
	}
	
	
	@When("^The user clicks on to the edit button for an order to be removed$")
	public void TheUserClicksOnToTheEditButtonForAnOrderToBeRemoved() throws InterruptedException {	
		Thread.sleep(2000);
		driver.findElement(By.id("Order9")).click(); 	
		Thread.sleep(1000);
	}	
	
	
	@Then("^The user sees the pop up stating they can delete$")
	public void TheUserSeesThePopUpStatingTheyCanDelete() throws InterruptedException {	
	Thread.sleep(2000);
	Alert alert = driver.switchTo().alert();
	String AlertMessage = driver.switchTo().alert().getText();
	System.out.print("****"+AlertMessage+"****");
	assertEquals(AlertMessage,"Your order has been deleted" );
	Thread.sleep(2000);
	alert.accept();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
