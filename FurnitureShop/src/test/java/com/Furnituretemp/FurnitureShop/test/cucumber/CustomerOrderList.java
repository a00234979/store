package com.Furnituretemp.FurnitureShop.test.cucumber;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CustomerOrderList  {
	
	private WebDriver driver = Hooks.driver;
	

	@Given("^the Customer is logged in$")
	public void loginScreen() {
		driver.get("http://localhost:8080/FurnitureShop/index.html"); 	
		driver.findElement(By.id("username")).sendKeys("Joe");   
		driver.findElement(By.id("password")).sendKeys("password");
		driver.findElement(By.id("submitBTN")).click(); 

	}
	
	@Then("^The user selects the Shopping tab$")
	public void TheUserSelectsTheShoppingTab () {
		
	driver.findElement(By.id("shoppingTab")).click(); 
	
	} 
	
	@Then("^The user should be able to see the list of items$")
	public void TheUserShouldBeAbleToSeeTheListOfItems() {	
		WebElement statusMessage = driver.findElement(By.id("ShoppingTitle")); 
		assertEquals(statusMessage.getText(),"Shopping");
	}		

}
