package com.Furnituretemp.FurnitureShop.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.FurnitureShop.data.UserDAO;
import com.FurnitureShop.model.User;
import com.FurnitureShop.rest.JaxRsActivator;
import com.FurnitureShop.rest.UserWS;
import com.Furnituretemp.FurnitureShop.test.utils.UtilsDAO;



	//	@FixMethodOrder(MethodSorters.NAME_ASCENDING)
		@RunWith(Arquillian.class)
		public class UserDAOWSTest {
			
			@Deployment
			public static Archive<?> createTestArchive() {
				return ShrinkWrap
						.create(JavaArchive.class, "Test.jar")
						.addClasses(UserDAO.class, User.class,
								JaxRsActivator.class,UserWS.class,
								UtilsDAO.class)
					//	.addPackage(EventCause.class.getPackage())
					//	.addPackage(EventCauseDAO.class.getPackage())
								//this line will pick up the production db
						.addAsManifestResource("META-INF/persistence.xml",
								"persistence.xml")
						.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

			}

			 
			@EJB 
			private UserWS userWS;
			
			@EJB
			private UserDAO userDAO;
			
			@EJB
			private UtilsDAO utilsDAO;
			
			 
			@Before
			public void setUp() {

				utilsDAO.deleteUserTable();
				
				User user= new User();
				user.setEmail("joe@email.com");
				user.setPassword("password");
				user.setUser_type("Customer");
				user.setUsername("Joe");
				userDAO.save(user);
	
				User user2 = new User();
				user2.setEmail("bridget@email.com");
				user2.setPassword("password");
				user2.setUser_type("Picker");
				user2.setUsername("Bridget");
				userDAO.save(user2);
			}
			
			@Test
			public void testGetAllUsers() {
				List<User> userList = userDAO.getAllUsers();
				assertEquals("Data fetch = data persisted", userList.size(), 2);
				assertEquals(userWS.findAllUsers().getStatus(), 200);
			}		
			
				
			@Test
			public void testGetUserById() {
				User user = userDAO.getUser(1);
				
				assertEquals(user.getEmail(), "joe@email.com");
				assertEquals(user.getPassword(), "password");
				assertEquals(user.getUser_type(), "Customer");
				assertEquals(user.getUsername(), "Joe");
				
				assertEquals(userWS.findUserByID(1).getStatus(), 200);
			}	
			
			@Test
			public void testGetUserByName() {
				List<User> user = userDAO.getUserByExactName("Joe");
				
				for(User el:user) {
					assertEquals(el.getId(),1);
					assertEquals(el.getUsername(), "Joe");
					assertEquals(el.getPassword(), "password");
					assertEquals(el.getUser_type(), "Customer");								
				}
			}		
			
			@Test
			public void testGetUserByNameWS() {
				List<User> user = userWS.findByExactName("joe");
				for(User el:user) {
					assertEquals(el.getId(),1);
					assertEquals(el.getUsername(), "Joe");
					assertEquals(el.getPassword(), "password");
					assertEquals(el.getUser_type(), "Customer");								
				}
			
			}				
}
 