package com.Furnituretemp.FurnitureShop.test.cucumber;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.*;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SelectingAnItemToOrder  {
	
	private WebDriver driver = Hooks.driver;
	

	@Given("^the Customer is logged in their account$")
	public void loginScreen() {
		driver.get("http://localhost:8080/FurnitureShop/index.html"); 	
		driver.findElement(By.id("username")).sendKeys("Joe");   
		driver.findElement(By.id("password")).sendKeys("password");
		driver.findElement(By.id("submitBTN")).click(); 

	}
	
	@And("^is on the shopping tab$")
	public void isOnTheShoppingTab(){

	driver.findElement(By.id("shoppingTab")).click(); 
	
	} 
	
	@When("^The user presses the buy button of an Item in the Shopping List$")
	public void TheUserShouldBeAbleToSeeTheListOfItemsonScreen() {	
		driver.findElement(By.id("Item1")).click();
	}		

	@When("^The user should see the a screen displaying items details$")
	public void TheUserShouldSeeTheAScreenDisplayingItemsDetails() throws InterruptedException {	
		Thread.sleep(1000);
		WebElement statusMessage = driver.findElement(By.id("modalTitle")); 
		assertEquals(statusMessage.getText(),"Order Confirmation");
	}		
	
}

