package com.Furnituretemp.FurnitureShop.test.cucumber;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.*;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PickerUpdatingAnOrder  {
	
	private WebDriver driver = Hooks.driver;
	

	@Given("^the Picker is logged in$")
	public void loginScreen() {
		driver.get("http://localhost:8080/FurnitureShop/index.html"); 	
		driver.findElement(By.id("username")).sendKeys("Bridget");   
		driver.findElement(By.id("password")).sendKeys("password");
		driver.findElement(By.id("submitBTN")).click(); 

	}
	

	@When("^The picker select the edit button for a customer order$")
	public void ThePickerSelectTheEditButtonForACustomerOrder() throws InterruptedException {	
		Thread.sleep(2000);
		driver.findElement(By.id("CusOrder10")).click(); 	
		Thread.sleep(2000);
	}		

	@Then("^The order will be displayed$")
	public void TheOrderWillBeDisplayed() {	
	String message = driver.findElement(By.id("ModalCusOrder")).getText();
	assertEquals(message,"Order Details" );	
	}	

	@When("^The picker select the close button$")
	public void ThePickerSelectTheCloseButton() throws InterruptedException {	
		Thread.sleep(1000);
		driver.findElement(By.id("PickerAlterClose")).click(); 	
		Thread.sleep(1000);
	}		
	
	@Then("^The Customer orderLists will be displayed$")
	public void TheCustomerOrderListsWillBeDisplayed() {	
	String message = driver.findElement(By.id("Pickers")).getText();
	assertEquals(message,"Pickers" );	
	}	
	
	
	@When("^The picker clicks on Processed$")
	public void ThePickerClicksOnProcessed() throws InterruptedException {	
		Thread.sleep(3000);
		driver.findElement(By.id("alterprocess")).click(); 	
		Thread.sleep(3000);
	}	
	
	@Then("^The picker will see an alert stating order has been updated$")
	public void ThePickerWillSeeAnAlertStatingOrderHasBeenUpdated() throws InterruptedException {	
	Thread.sleep(2000);
	Alert alert = driver.switchTo().alert();
	String AlertMessage = driver.switchTo().alert().getText();
	System.out.print("****"+AlertMessage+"****");
	assertEquals(AlertMessage,"The status of the Order has been updated." );
	Thread.sleep(1000);
	alert.accept();
	}	
	
	
	
	
//	@When("^The user clicks on to the edit button for an order$")
//	public void TheUserClicksOnToTheEditButtonForAnOrder() throws InterruptedException {	
//		Thread.sleep(1000);
//		driver.findElement(By.id("Order1")).click(); 	
//		Thread.sleep(1000);
//	}
//	
//	@And("^The user clicks cancelOrder$")
//	public void TheUserClicksCancelOrder() throws InterruptedException {	
//		Thread.sleep(1000);
//		driver.findElement(By.id("CancelOrder")).click(); 	
//		Thread.sleep(1000);
//	}	
//	
//	
//	@Then("^The user sees the pop up stating they can not alter order$")
//	public void TheUserSeesThePopUpStatingTheyCanNotAlterOrder() throws InterruptedException {	
//	Thread.sleep(3000);
//	Alert alert = driver.switchTo().alert();
//	String AlertMessage = driver.switchTo().alert().getText();
//	System.out.print("****"+AlertMessage+"****");
//	assertEquals(AlertMessage,"Cutomer Notice , You can no longer alter this order" );
//	Thread.sleep(3000);
//	alert.accept();
//	}
//	
//	
//	@When("^The user clicks on to the edit button for an order to be removed$")
//	public void TheUserClicksOnToTheEditButtonForAnOrderToBeRemoved() throws InterruptedException {	
//		Thread.sleep(2000);
//		driver.findElement(By.id("Order2")).click(); 	
//		Thread.sleep(1000);
//	}	
//	
//	
//	@Then("^The user sees the pop up stating they can delete$")
//	public void TheUserSeesThePopUpStatingTheyCanDelete() throws InterruptedException {	
//	Thread.sleep(3000);
//	Alert alert = driver.switchTo().alert();
//	String AlertMessage = driver.switchTo().alert().getText();
//	System.out.print("****"+AlertMessage+"****");
//	assertEquals(AlertMessage,"Your order has been deleted" );
//	Thread.sleep(3000);
//	alert.accept();
//	}
//	
//
//	
//	
	
	
	
	
	
	


}
