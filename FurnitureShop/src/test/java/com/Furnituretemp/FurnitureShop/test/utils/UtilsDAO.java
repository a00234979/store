package com.Furnituretemp.FurnitureShop.test.utils;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@LocalBean
public class UtilsDAO {

    @PersistenceContext
    private EntityManager em;

	public void deleteUserTable() {
		em.createQuery("DELETE FROM User").executeUpdate();
		em.createNativeQuery("ALTER TABLE User AUTO_INCREMENT=1").executeUpdate();
	}
	
	public void deletePlastersDataTable() {
		em.createQuery("DELETE FROM Plasters").executeUpdate();
		em.createNativeQuery("ALTER TABLE Plasters AUTO_INCREMENT=1").executeUpdate();
	}
	 
	public void deletePlasters_orderTable() {
		em.createQuery("DELETE FROM Plaster_Order").executeUpdate();
		em.createNativeQuery("ALTER TABLE Plaster_Order AUTO_INCREMENT=1").executeUpdate();
	}      
}
